FROM node:16-alpine as build
WORKDIR /opt/
COPY package.json package-lock.json ./
RUN npm install && npm install react-scripts@4.0.3 -g
COPY . .
RUN npm run build

FROM nginx:latest 
ARG REACT_APP_PUBLIC_URL $REACT_APP_PUBLIC_URL
COPY --from=build /opt/build /usr/share/nginx/html

EXPOSE 80
CMD [ "nginx", "-g", "daemon off;" ]


